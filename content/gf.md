---
title: "Gravity Falls"
image: /images/GFCapa.jpg
image2: /images/GFS.jpg
image3: /images/GFP.jpg
date: 2021-09-18T16:59:34-03:00
draft: false
---

Essa é uma série de televisão animada americana criada por Alex Hirsch, produzida pela Disney Television Animation originalmente para o Disney Channel (e depois para a Disney XD). Ela é uma animação digital. 

 ||||||||||

## Gêneros

 ||||||||||

Animação; Comedia; Aventura; Suspense. 

 ||||||||||

## Plot

 ||||||||||

A série segue as aventuras de Dipper Pines e sua irmã gêmea Mabel de 12 anos, que são enviadas para passar suas férias de verão com seu tio-avô Stanford Pines, em Gravity Falls, uma cidade misteriosa cheia de forças paranormais e criaturas sobrenaturais.
As crianças ajudam Stan a administrar "A Cabana do Mistério", a armadilha turística que ele possui, enquanto também investigam os verdadeiros mistérios do local. Nada é o que parece nessa cidade, e com ajuda de um diário misterioso que Dipper acha na floresta, eles começam a desvendar os mistérios locais.
Com participações de Wendy Corduroy, adolescente de 15 anos que trabalha como caixa na Cabana do Mistério e por quem Dipper nutre uma paixão secreta; Soos Ramirez, amigo dos gêmeos que trabalha para Ti-vô Stan/Tio Stan; além de uma variedade de outros personagens eventuais (que acabam não sendo tão secundários quanto parecem a princípio); Dipper e Mabel sempre têm um dia estranho.

||||||||||

## Personagens principais

 ||||||||||

Mabel Pines, Dipper Pines, Wendy, Stanley Pines, Robbie, Waddles, Ford Pines, Gideon Gleeful, Pacifica, Soos e Bill.
