---
title: "Over The Garden Wall"
image: /images/OGWCapa.jpg
image2: /images/OGWS.jpg
image3: /images/OGWP.jpg
date: 2021-09-18T23:28:40-03:00
draft: false
---

O Segredo Além do Jardim é uma série de desenho animado estadunidense em minissérie criada por Patrick McHale e produzida e distribuída pela Cartoon Network. O programa foi a primeira minissérie do canal, que iniciou a sua produção em março de 2014 e é uma animação digital.

 ||||||||||

## Gênero

 ||||||||||

Minissérie; Aventura; Comédia dramática; Dark fantasy; Musical

 ||||||||||

## Plot

 ||||||||||

A série foca nos irmãos, Wirt e Greg, que se veem no Desconhecido, uma estranha floresta.[4][5] Para encontrar o caminho de casa, eles viajam por toda a floresta e pelas cidades que passam, com a ajuda do sábio velho Woodsman (Lenhador[4] ou O Senhor da Mata), e Beatrice a pássara azul que os acompanha para desfazer a maldição que afetou toda a sua família.

||||||||||

## Personagens Principais

 ||||||||||
 
Wirt, Greg, The woodsman, beatrice, Quincy Endicott, Margueritte Grey, John Crops