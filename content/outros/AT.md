---
title: "Adventure Time"
image: /images/ATCap.jpg
image2: /images/ATS.jpg
image3: /images/ADP.jpg

date: 2021-09-18T23:27:57-03:00
draft: false
---

Hora de Aventuras é uma série de desenho animado americana criada por Pendleton Ward para o Cartoon Network. Essa é uma computação gráfica. 

 ||||||||||

## Gênero

 ||||||||||

Ação; Aventura; Animação; Comédia; Fantasia; Ficção científica.

 ||||||||||

## Plot

 ||||||||||

 A série segue as aventuras de Finn  um garoto humano aventureiro, e o seu melhor amigo e irmão adotivo Jake, um cão com poderes que lhe permitem alterar a forma e tamanho conforme a sua vontade. Finn e Jake habitam a pós-apocalíptica Terra de Ooo, onde interagem com os outros personagens principais da série: Princesa Jujuba, o Rei Gelado e Marceline, a Rainha dos Vampiros.

 ||||||||||

## Personagens Principais

 ||||||||||
 
Finn, Jake, Marceline, BMO, Rei Gelado, Princesa de Fogo, Princesa Jujuba, Princeça Caroço, Earl of Lemongrab, Dona Tromba e Lich

