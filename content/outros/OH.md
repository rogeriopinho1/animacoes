---
title: "The Owl House"
image: /images/OHCapa.jpg
image2: /images/OHS.jpeg
image3: /images/OHP.jpg
date: 2021-09-18T23:28:40-03:00
draft: false
---

Ou A Casa da Coruja, é uma série de televisão de comédia, LGBT e fantasia estadunidense animada criada por Dana Terrace e produzida pela Disney Television Animation para o Disney Channel. Essa também é uma animação digital.

 ||||||||||

## Gênero

 ||||||||||

Animação; Comédia; Fantasia.

 ||||||||||

## Plot

 ||||||||||

The Owl House é uma série de comédia e que acompanha Luz, uma adolescente humana auto-confiante que acidentalmente se depara com um portal para o Reino Demoníaco. Lá ela faz amizade com uma bruxa rebelde, Eda, e um guerreiro adoravelmente pequeno, King. Apesar de não ter habilidades mágicas, Luz persegue o seu sonho de se tornar uma bruxa, sendo aprendiz de Eda na Casa da Coruja e, finalmente, encontra uma nova família num ambiente improvável.

||||||||||

## Personagens principais

 ||||||||||
 
Luz Noceda, Edalyn Clawthorne, Amity Blight, Raine Whispers, Gus Porter, Willow Park, Lilith Clawthorne e King,