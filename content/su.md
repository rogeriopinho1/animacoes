---
title: "Steven Universe"
image: /images/SUCapa.png
image2: /images/SUP.png
image3: /images/SUV.png
date: 2021-09-18T16:59:34-03:00
draft: false
---

Essa é uma série de desenho animado norte-americano criada por Rebecca Sugar para o canal televisivo animado Cartoon Network. Esse é uma animção "animatic", que nada mais é do que uma mistura de computação gráfica, ilustração, animação vetorial e composição.

 ||||||||||

## Gêneros

 ||||||||||

Ação; Animação; Aventura; Comédia; Drama; Fantasia; Ficção científica; Musical; Suspense; Mistério

 ||||||||||

## Plot

 ||||||||||

É a história de amadurecimento de um menino, Steven Universe, que mora com as Crystal Gems - seres intergalácticos mágicos. As chamadas Garnet, Pérola e Ametista - na cidade fictícia de Beach City. Steven, que é meio Gem, meio humano, tem aventuras com seus amigos e ajuda as Crystal Gems a proteger o mundo de sua própria espécie e as Gems corrompidas do Planeta Natal.

||||||||||

## Personagens principais

 ||||||||||
 
Garnet, Amethyst, Pérola, Steven Universo, Connie, Lapis Lazuli, Stevonnie e Peridot.

